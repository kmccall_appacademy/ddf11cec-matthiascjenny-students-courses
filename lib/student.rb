require 'course'

class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name,last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    return "#{@first_name} #{@last_name}"
  end

  def courses
    return @courses
  end

  def enroll(course)
    if @courses.any? { |c| course.conflicts_with?(c) }
      raise_error
    else
      unless @courses.include?(course)
        @courses.push(course)
        course.students.push(self)
      end
    end
  end

  def course_load
    hsh = Hash.new(0)
    @courses.each do |c|
      hsh[c.department] += c.credits
    end
    return hsh
  end

end
